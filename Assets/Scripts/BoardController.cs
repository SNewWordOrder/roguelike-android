﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardController : MonoBehaviour {
	public int columns;
	public int rows;

	public GameObject[] floors;
	public GameObject[] outerWalls;
	public GameObject[] wallObstacles;
	public GameObject[] foodItems;
	public GameObject[] enemies;
	public GameObject exit;

	private Transform gameBoard;
	private List<Vector3>ObstaclesGrid;
	void Awake () {
		ObstaclesGrid = new List<Vector3> ();
	}
	

	void Update () {
	
	}
	private void InitializeObstaclePositions() {
		ObstaclesGrid.Clear ();
		for (int x=2; x<columns-2; x++) {
			for (int y = 2; y<rows-2; y++) {
				ObstaclesGrid.Add(new Vector3(x,y,0f));
			}

		}


	}

	private void SetupGameBoard() {
		gameBoard = new GameObject("Game Board").transform;
		for (int x=0; x<columns; x++) {
			for(int y = 0;y<rows; y++) {
				GameObject selectedTile;
				if ( x== 0 || y == 0 || x == columns -1 || y == rows - 1  ){
					selectedTile = outerWalls[Random.Range (0,outerWalls.Length)];
				}
				else {
			 selectedTile= floors[Random.Range (0, floors.Length)];
				}
				GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
				floorTile.transform.SetParent(gameBoard);
			}
		}
	}

	private void SetRandomObstaclesGrid(GameObject[] obstaclesArray, int minimum, int maximum)
	{
		int obstacleCount = Random.Range (minimum, maximum + 1);

		if (obstacleCount > ObstaclesGrid.Count) {
			obstacleCount = ObstaclesGrid.Count;
		}
		for (int index = 0; index<obstacleCount; index++) {
			GameObject selectedObstacle = obstaclesArray[Random.Range(0,obstaclesArray.Length )];
			Instantiate(selectedObstacle,SelectGridPPosition(),Quaternion.identity);
		}
		}
	private  Vector3 SelectGridPPosition(){
		int randomIndex = Random.Range(0,ObstaclesGrid.Count);
		Vector3 randomPosistion = ObstaclesGrid [randomIndex];
		ObstaclesGrid.RemoveAt(randomIndex);
		return randomPosistion;
	}

	public void setUpLevel(int currentLevel){
		InitializeObstaclePositions ();

		SetupGameBoard ();
		SetRandomObstaclesGrid (wallObstacles, 3, 9);
		SetRandomObstaclesGrid (foodItems,1,5);
		int enemyCount = (int)Mathf.Log(currentLevel, 2);
		SetRandomObstaclesGrid (enemies,enemyCount, enemyCount);
		Instantiate (exit, new Vector3 (columns - 2, rows- 2,0f), Quaternion.identity);

	}
}
